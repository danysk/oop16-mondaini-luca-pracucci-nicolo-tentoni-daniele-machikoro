Programma per il progetto di Programmazione ad Oggetti.

E' lo sviluppo di un gioco da tavolo in versione digitale, in modo da azzerare o quanto meno ridurre i tempi di attesa che possono risultare lunghi. Alcuni di questi possono essere lo scambio di monete e il lancio dei dadi.

## Stato del progetto ##
Il Progetto è in piena fase di implementazione, si procede a un buon ritmo.
Ci sono ovviamente dei problemi, dei bug, le solite cose, che vengono risolte mano a mano che si procede nello sviluppo.
Non appena sarà pronta una piccola versione di prova verrà messa subito online come file JAR eseguibile.

## Sviluppatori ##
Gli sviluppatori sono:
1) Daniele Tentoni, capo-progetto, Model e IA
2) Nicolo' Pracucci, Controller
3) Luca Mondaini, View e Imaging